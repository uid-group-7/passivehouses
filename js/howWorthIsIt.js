function calculateWorth(){
    
    // The input fields
    var houseSurfaceInput = document.getElementById("houseSurface");
    var budgetInput = document.getElementById("budget");
    var monthlyConsumptionInput = document.getElementById("monthlyEnergyConsumption")

    // The values in the input fields
    var houseSurface = houseSurfaceInput.value;
    var budget = budgetInput.value;
    var monthlyConsumption = monthlyConsumptionInput.value;

    var worth = houseSurface * monthlyConsumption / budget;
    var index;

    if (worth <= 0.2 && worth > 0) {
        worth = 90;
    } else if (worth <= 0.5 && worth > 0.2) {
        worth = 75;
    } else if (worth <= 0.7 && worth > 0.5) {
        worth = 50;
    } else if (worth <= 0.9 && worth > 0.7) {
        worth = 35;
    } else if (worth <= 0.9 && worth > 0.7) {
        worth = 20;
    } else {
        worth = 10;
    }

    var request = new XMLHttpRequest();
    request.open('GET', 'http://localhost:3000/' + worth, true);
    request.onload = function () {
        var data = JSON.parse(this.response);
        if (request.status >= 200 && request.status < 400) {
            var recomendation = document.getElementById("recommendationText");
            recomendation.innerHTML = "";
            recomendation.innerHTML += data.recommendation;
            move(worth);
        } else {
            console.log('error');
        }
    }
    request.send();

}

function move(worth) {
    var elem = document.getElementById("myBar");
    var width = 0;
    var id = setInterval(frame, 16);
    function frame() {
      if (width >= worth) {
        clearInterval(id);
      } else {
        width++;
        elem.style.width = width + '%';
        elem.innerHTML = width * 1 + '%';
      }
    }
  } 