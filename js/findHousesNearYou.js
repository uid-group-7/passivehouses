function obtainHouses(){
    
    // The selectors and the inputs
    var countrySelector = document.getElementById("country");
    var citySelector = document.getElementById("city");
    var roomsInput = document.getElementById("rooms");
    var budgetInput = document.getElementById("budget");
    var monthlyConsumptionInput = document.getElementById("monthlyEnergyProduction");

    // The actual values
    var country = countrySelector.options[countrySelector.selectedIndex].value;
    var city = citySelector.options[citySelector.selectedIndex].value;
    var rooms = roomsInput.value;
    var budget = budgetInput.value;
    var monthlyConsumption = monthlyConsumptionInput.value;

    var validData = validateAllNumberFields(roomsInput, budgetInput, monthlyConsumptionInput, "roomsError", "budgetError", "monthlyEnergyProductionError");
    if (validData){
        getHousesFor(city, rooms, budget, monthlyConsumption);
    }
}

function getHousesFor(city, rooms, budget, monthlyConsumption){
    var request = new XMLHttpRequest();
    request.open('GET', 'http://localhost:3000/' + city, true);
    request.onload = function () {
        var data = JSON.parse(this.response);
        if (request.status >= 200 && request.status < 400) {
            var houseList = document.getElementById("houseList");
            houseList.innerHTML = "";
            for (i in data){
                var houseOffer = data[i];
                if (houseOffer.budget <= budget &&
                    houseOffer.rooms >= rooms &&
                    houseOffer.production >= monthlyConsumption){
                        console.log("motha");
                        houseList.innerHTML += 
                            "<li value=\"1\"> " +
                                "<div class=\"house\"> " +
                                    "<img src=" + houseOffer.picture+">" +  
                                    "<div class=houseText> " + 
                                        "<p><b>"+ houseOffer.title +"</b></p> " + 
                                        "<p>"+ houseOffer.subTitle +"</p> " + 
                                        "<p>"+ houseOffer.description +"</p>" +
                                    "</div>" +
                                "</div>" +
                            "</li>";
                    }
            }
        } else {
            console.log('error');
        }
    }
    request.send();
}

function validateAllNumberFields(roomsInput, budgetInput, monthlyConsumptionInput, roomError, budgetError, monthlyConsumptionError){
    if (validateNumbersOnly(roomsInput, roomError) && validateNumbersOnly(budgetInput, budgetError) && validateNumbersOnly(monthlyConsumptionInput, monthlyConsumptionError)){
        return true;
    }
    return false;
}

function validateNumbersOnly(field, errorField){
    var errorField = document.getElementById(errorField);
    var numbers = /^[0-9]+$/;
      if(field.value.match(numbers)) {
        errorField.innerHTML = "";
        return true;
      } else if (field.value === ""){
        errorField.innerHTML = "This field cannot be empty";
        return false;     
      }
      else {
        errorField.innerHTML = "Only numbers are allowed!";
        return false;
      }
}
