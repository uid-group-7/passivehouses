window.onload = init;
var rIndex;

function init(){
	var addbutton = document.getElementById("addbtn");
	var editbutton = document.getElementById("editbtn");
	var removebutton = document.getElementById("yes");
	var calcbutton = document.getElementById("calcbtn");
	addbutton.onclick = addHtmlTableRow;
	editbutton.onclick = editHtmlTbleSelectedRow;
	removebutton.onclick = removeSelectedRow;
	calcbutton.onclick = calculatePanels;
}

	function checkEmptyInput() {
	    var isEmpty = false;
	    var appliance = document.getElementById("appliance").value;
	    var consumption = document.getElementById("consumption").value;
		if(appliance === ""){
	        alert("First Name Connot Be Empty");
	        isEmpty = true;
	    }
	    else if(consumption === ""){
	        alert("Last Name Connot Be Empty");
	        isEmpty = true;
	    }
	    return isEmpty;
	}

	// add Row
	function addHtmlTableRow() {
		var table = document.getElementById("apptable");
		if(!checkEmptyInput()){
	        var newRow = table.insertRow(table.length),
	        cell1 = newRow.insertCell(0),
	        cell2 = newRow.insertCell(1),
	        appliance = document.getElementById("appliance").value,
	        consumption = document.getElementById("consumption").value;
	        cell1.innerHTML = appliance;
	        cell2.innerHTML = consumption;
	        selectedRowToInput();
	    }
	}

	// display selected row data into input text
	function selectedRowToInput() {
		var table = document.getElementById("apptable");
		for(var i = 1; i < table.rows.length; i++) {
	        table.rows[i].onclick = function(){
	                      rIndex = this.rowIndex;
	                      document.getElementById("appliance").value = this.cells[0].innerHTML;
	                      document.getElementById("consumption").value = this.cells[1].innerHTML;
	                    };
	    }
	}
	            
	function editHtmlTbleSelectedRow() {
	var table = document.getElementById("apptable");
	    var appliance = document.getElementById("appliance").value;
	    var consumption = document.getElementById("consumption").value;
	    if(!checkEmptyInput()){
	        table.rows[rIndex].cells[0].innerHTML = appliance;
	        table.rows[rIndex].cells[1].innerHTML = consumption;
	    }
	}
	            
	function removeSelectedRow() {
		var table = document.getElementById("apptable");
	    table.deleteRow(rIndex);
		document.getElementById("appliance").value = "";
	    document.getElementById("consumption").value = "";
	}

function calculatePanels(){
	var table = document.getElementById("apptable");
	var sum = 0;
	for(var i = 1; i < table.rows.length; i++) {
		sum += parseInt(table.rows[i].cells[1].innerHTML);
	}
	sum /= 365 * 0.5;
	var text = document.getElementById("nr-panels");
	var string = "YOU NEED " + Math.ceil(sum) + "\n" + "SOLAR PANELS";
	text.innerText = string;
}
