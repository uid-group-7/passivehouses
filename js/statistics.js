$(document).ready(function() {
  /*$("#statButton").click(function()
  {
    $("#devicesPart").hide();

  });*/

  var modal = document.getElementById('myModal');

  var btn = document.getElementById("statButton");

  // Get the <span> element that closes the modal
  var span = document.getElementsByClassName("close")[0];

  // When the user clicks on the button, open the modal
  btn.onclick = function() {
    modal.style.display = "block";
  }

  // When the user clicks on <span> (x), close the modal
  span.onclick = function() {
    modal.style.display = "none";
  }

  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
  }

  var ctx1 = $("#pie-chartcanvas-1");

  var data1 = {
   labels: ["LG B8", "Dyson Pure Cool", "MENRED Home wall Solar power"],
   datasets: [
     {
       label: "TeamA Score",
       data: [55,30,15],
       backgroundColor: [
         "#f48942",
         "#6d2edb",
         "#20c13e",

       ],
       borderColor: [
         "#CDA776",
         "#989898",
         "#CB252B",
       ],
       borderWidth: [1, 1, 1]
     }
   ]
 };

 var options = {
    responsive: true,
    title: {
      display: true,
      position: "top",
      text: "Power Consumption Of The Devices",
      fontSize: 14,
      fontColor: "#111"
    },
    legend: {
      display: true,
      position: "bottom",
      labels: {
        fontColor: "#333",
        fontSize: 12
      }
    }
  };

  var chart1 = new Chart(ctx1, {
    type: "pie",
    data: data1,
    options: options
  });

  $("#close").click(function()
{
    modal.style.display = "none";
});

  $("#save").click(function()
{
    modal.style.display = "none";
});

});
