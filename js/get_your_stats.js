window.onload = findDevices;

function findDevices() {
    var request = new XMLHttpRequest();
    request.open('GET', 'http://localhost:3000/' + "devices", true);
    request.onload = function () {
        var data = JSON.parse(this.response);
        if (request.status >= 200 && request.status < 400) {
            var devicesTable = document.getElementById("devicesTable");
            for (i in data){
                var device = data[i];
                console.log(device);
                devicesTable.innerHTML +=
                "<tr>" 
                    +"<td class=\"image-td\">" 
                    +    "<img src=" + device.icon + ">" 
                    +"</td>" 
                    +"<td class=\"name-td\">"  
                    +    "<p>" + device.name + "</p>" 
                    +"</td>" 
                    +"<td id="+ device.consumption + " class=\"description-td\">"
                    +    "<p>" + device.description + "</p>"
                    +"</td>" 
                +"</tr>";
            }
        } else {
            console.log('error');
        }
    }
    request.send();
}


function seeStats(){
    var modal = document.getElementById("myModal");
    var ctx1 = $("#pie-chartcanvas-1");
    $("#myModal").modal("show");
    var data1 = {
     labels: ["LG B8", "Dyson Pure Cool", "MENRED Home wall Solar power"],
     datasets: [
       {
         label: "TeamA Score",
         data: [55,30,15],
         backgroundColor: [
           "#f48942",
           "#6d2edb",
           "#20c13e",
  
         ],
         borderColor: [
           "#CDA776",
           "#989898",
           "#CB252B",
         ],
         borderWidth: [1, 1, 1]
       }
     ]
   };
  
   var options = {
      responsive: true,
      title: {
        display: true,
        position: "top",
        text: "Power Consumption Of The Devices",
        fontSize: 14,
        fontColor: "#111"
      },
      legend: {
        display: true,
        position: "bottom",
        labels: {
          fontColor: "#333",
          fontSize: 12
        }
      }
    };
  
    var chart1 = new Chart(ctx1, {
      type: "pie",
      data: data1,
      options: options
    });
  
    
}
