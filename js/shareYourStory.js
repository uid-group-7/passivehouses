function saveUserStory() {
    var modalHeaderText = document.getElementById("editableModalHeader").innerText;
    var modalBodyText = document.getElementById("editableModalBody").innerText;
    var parent = document.getElementById("scrollableContent");
    var firstStoryComponent = document.getElementById("firstStoryComponent");

    var divParentContainer = document.createElement('div');
    divParentContainer.setAttribute("class", "story-component span4 proj-div");
    divParentContainer.setAttribute("data-toggle", "modal");
    divParentContainer.setAttribute("data-target", "#GSCCModal6");

    var divContainer = document.createElement('div');
    divContainer.setAttribute("class", "userStoryPreview");

    var image = document.createElement('img');
    image.setAttribute("src", "../resources/genericUserPhoto.jpeg");
    image.setAttribute("class", "userImage");

    var text = document.createElement('p');
    text.innerHTML = modalBodyText;

    var title = document.createElement('h4');
    title.innerText = modalHeaderText;

    divContainer.appendChild(title);
    divContainer.appendChild(text);

    divParentContainer.appendChild(image);
    divParentContainer.appendChild(divContainer);

    parent.insertBefore(divParentContainer, firstStoryComponent);

    document.getElementById("newModal").innerHTML += '<div id="GSCCModal6" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">\n' +
        '    <div class="modal-dialog">\n' +
        '        <div class="modal-content">\n' +
        '            <div class="modal-header">\n' +
        '                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n' +
        '                <h4 class="modal-title">' + modalHeaderText + '</h4>\n' +
        '            </div>\n' +
        '            <div class="modal-body">\n' +
                        modalBodyText +
        '            </div>\n' +
        '            <div class="modal-footer">\n' +
        '                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </div>\n' +
        '</div>';

}

function story1() {
    document.write('This guest post from Ian is part of the “reader stories” feature at Get Rich Slowly. It\'s the extended version of the story he shared in his prize-winning entry to this year\'s GRS video contest. Some reader stories contain general advice; others are examples of how a GRS reader achieved financial success — or failure. These stories feature folks from all levels of financial maturity and with all sorts of incomes.'
        + 'It dawned on me in college, having experienced several different summer jobs, that I really didn\'t like being employed. Sure, the money is nice — but it\'s just no fun at all to spend your days working to reach some boss\'s plans or goals. I\'m sure there are some folks out there who find a 9-to-5 job fulfilling, but that sure ain\'t me. There\'s too much fascinating stuff out there to learn and do to spend 40 years in a cubicle. The mere thought makes me shudder, and I wanted nothing to do with a career. \n'
        + 'Most of the financial advice out there is geared towards building up a big account to retire on. I figured that I would enjoy taking a different route — reducing the total income I needed to live on. With a significant reduction in expenses, it becomes feasible to live very comfortably on a part-time income, or even just income from hobbies. How do you reduce your expenses that much? Live off the grid.'
        +  '\n\nPlanning\n'
        + 'By “live off the grid”, I don\'t mean abandoning all your possessions to live in a shack in the woods. I mean taking control of your necessities and providing them yourself instead of relying on other to do it for you (and paying them to do so). Going offgrid requires a greater up-front payment, which is rewarded by great benefits in the long term (sound familiar?). Building a house yourself is a huge investment in time, sweat, and cash — but it allows you to enjoy freedom from rent or mortgage for decades. Like cooking at home instead of going out, but writ large (hundreds of thousands of dollars large).'
        + 'Note: My decision to follow this path was not purely a financial one — I simply am happiest out in the boonies. There are too many people in the city, and it\'s just not enjoyable for me. I want some space. You may be different — and probably are.'
        + 'The more I looked at the offgrid option, the more financial advantages I saw in it. By choosing an earth-bermed home design, I could minimize heating and cooling expenses, as well as exterior maintenance. Having my own well and septic system eliminate the water bill, and having my own photovoltaic system for electricity cuts out another bill. My consumable fuels for the home are limited to some wood for winter heating (easily collected from the property) and propane for cooking (for which a couple hundred gallon tank is nearly a lifetime supply). Add some food production on the land, and you can also reduce grocery expenses.'
        + 'Does this mean intentional poverty? Absolutely not. It means that I can have great quality of life, make $10,000 per year with a part-time or online gig, and have more disposable income than most middle income debt-ridden wage slaves.'
        + '\n\nExecution\n'
        + 'At the time I put this notion together, I was in the middle of getting a fancy engineering degree from a fancy university. I had been losing interest in engineering as a field to work in, and opted to jump to a more hands-on field of study and get the fastest two-year degree I could. I judged that it would be better to leave with some sort of diploma than drop out altogether.'
        + 'At the same time, I started looking for affordable rural land. I had a small inheritance from a great grandparent that I had been saving for something significant and meaningful, and a piece of land seemed like the perfect use for it. I eventually found a 40 acre parcel in the Southwest for less than $500/acre. I ditched school for a week to camp out on it, and fell in love. It had a good southeast facing slope for my passive solar house plan, and everything else I wanted in a parcel.');
}

function story2() {
    document.write('We continued our Engineering theme with a class about building houses.\n' +
        '\n' +
        'Question of the Day: What are the steps in building a house?\n' +
        'Challenge: Can you build a house with friends?\n' +
        '\n' +
        'Incorporating Books into the Lesson: We had three different books that are about the process of building a house.\n' +
        '\n' +
        'Building a House by Barton is a nice simple book for ages 3 to 5 about each of the steps in building a house.\n' +
        'Building Our House by Bean is great for ages 4 to 6. It tells the story of a family building their own house. Told from the perspective of a young child.\n' +
        'How a House Is Built by Gibbons – I find some of the details in this book are more than a child age 5 – 8 would care about, but the illustrations are good.\n' +
        'We photocopied and printed copies of many of the pages, and posted those pictures near the activity related to that step in the book to help give context to the activities we were doing. We later used these books in circle time, where they wove together the story of the separate activities into a whole process. Here is a pdf of the pages from Barton.\n' +
        '\n' +
        'Note: If you would like to have a full curriculum based on one book, I have an alternative lesson plan for building a house that is all tightly focused around the Barton book, with activities to match virtually every page. Check it out here.\n' +
        '\n' +
        'Activities – Stages of House Construction\n' +
        '\n' +
        'The Design Process – Blueprints (Crayon Resist Art). We set out white crayons and white oil pastels so they could draw floor plans or house designs or whatever they wanted, and then paint over them with thinned blue tempera paint and a roller brush. (Idea from No Time for Flashcards.) We put up pictures of blue prints from our books.\n' +
        '\n' +
        'Excavation – the sensory table: One year, we had sand, buckets and shovels. This year, we put out brown cloud dough  – it looks like rich, brown dirt, but it’s made of flour, cocoa powder, and vegetable oil, so it smells good and is a good option for a sensory bin if you have little ones who might eat dirt. We put in a collection of toy bulldozers and dump trucks. We posted illustrations of bulldozers excavating house sites.\n' +
        '\n' +
        'Framing – Build a House! This is one of my all-time favorite collaborative activities for kids age 3 – 7. I first saw it at a construction themed birthday party. (Read more here: https://gooddayswithkids.com/2014/12/18/kids-construction/.)\n' +
        '\n' +
        'You use foam insulation panels (we use the 1 inch by 2 foot by 4 foot panels from Home Depot – for this class, I had nine full panels, plus a few cut into halves and thirds. Most were re-used from the last two times we did this project) plus golf tees and toy hammers to assemble a building. Kids LOVE to hammer and feel like they’re building “something real.” It can keep them entertained for hours. Here are some of the buildings my students have built. Click on any picture for a bigger view.\n');
}

function story3() {
    document.write('Should you build a one-story or two-story home? How do you choose one over the other? Aside from your personal opinion on whether you prefer stairs — or not — there are some practical advantages and limitations to both. So let’s look at them, starting with the two-story designs.\n' +
        '\n' +
        'Advantages of Two Story Homes\n' +
        'Cost less per square foot to build. That’s because the most expensive elements of home-building — excavation/foundation and rafters/roof installation — are being built on a smaller footprint. Plus, you’ll have less roof area to maintain.\n' +
        'More fuel efficient. You’ll save fuel because, per square foot, less outdoor wall and roof area are exposed to the weather.\n' +
        'Less distance utilities travel. You’ll save money (and potential headaches) because plumbing and wiring have less distance to travel.\n' +
        'Better views. When you are tree-height, your views are better.\n' +
        'Larger outdoor space. Since you are taking up less land space for the house, you’ll have more of it for outdoor living areas.\n' +
        'More versatile design. There are more attachment points in a two-story house for porches, connectors and bump-outs for either now or in the future.\n' +
        'Privacy. The upstairs bedrooms are more private, especially if you build in a neighborhood. If you have children, you’ll appreciate the second-floor bedrooms feature when your kids grow into teenagers.\n' +
        '"There are more attachment points in a two story house for porches, connectors and bump-outs for either now or future consideration."\n' +
        '\n' +
        'Disadvantages of Two Story Homes\n' +
        'The stairway’s footprint. A stairway can eat up 100 square feet of living space and add to the cost.\n' +
        'Lack of variety in ceiling height. Ceiling (and attic) heights are typically lower than single-floor homes of equal square footage and can lack varied heights room-to-room. This could limit opportunities for skylights.\n' +
        'Stair accidents. Stairs present a potential danger for young children, the elderly, or anyone with mobility issues. And if you ever need to install a stair lift, expect to shell out thousands of dollars.\n' +
        'Construction considerations. A two-story house takes longer to build because of the second (and sometimes third attic) floor, the added staircase and sometimes a deeper foundation.\n' +
        'Modern Long Island one story home\n' +
        'Contemporary timber frame two story home\n' +
        'Advantages of Single Story Home\n' +
        'More living space per square foot. You won’t be compromising square footage (and money) on staircases.\n' +
        'Safer than two-story homes. Take away the staircase, and you eliminate the risk of falls for small children, the mobility-challenged and the elderly. Plus, evacuation is easier (and safer) than a two-story home in the event of a fire.\n' +
        'Easier to “age in place.” If you plan to stay in your home until retirement, there may be no need to move. One-story houses are easier to get around and are easily wheelchair accessible.\n' +
        'Quieter living. You won’t hear footsteps or other noise coming from upstairs.\n' +
        'Space-saving design. From a practical point of view, you’ll need fewer bathrooms in a one-story home since they are all on the same floor, which can save you money. Also, the mudroom and laundry room can more easily be combined, saving space.\n' +
        'More design options. One-floor living allows more variation in ceiling heights and skylights.\n' +
        'Disadvantages of Single Story Home\n' +
        'Expect to pay more. Per square foot, a one-story house is more costly to build than a two-story home. There is a larger footprint, meaning more foundation building and more roofing materials. And because the plumbing and heating/AC systems need to extend the length of the house, you’ll need bigger (and costlier) systems.\n' +
        'Less privacy. One-floor living means that you are eye-level with the rest of the world passing by. Of course, if you live in the woods that may not be true, but one-story homes in neighborhoods may feel exposed and less private.\n' +
        'Resale value may be lower. Two-story homes, on average, command higher prices, because the demand among families is higher. But that may change as baby boomers face retirement (and decreased mobility) and those nearing retirement want to “age in place.” This higher demand for single-story homes could mean valuation could surpass those of their taller cousins.');
}

function story4() {
    document.write('Permit Filings\n' +
        'Today, virtually all communities require builders to file permits before constructing homes. Your local city or county inspector\'s office keeps a record of which builder pulled the permit, and often the names of the subcontractors as well. Many communities started archiving permit-filing records seven, or more, decades ago. If you live outside the city, contact the county offices to find which department maintains rural permits.\n' +
        '\n' +
        'Service Attachments\n' +
        'Most builders don\'t put their names on the homes they build, but you can sometimes track them down by contacting one of the subcontractors. Plumbers regularly put their service stickers on water heaters, reverse osmosis units and water softeners. Likewise, HVAC contractors put their service stickers on air conditioners, furnaces and even ductwork. If the subcontractor is still in business, he might remember who built your house.\n' +
        '\n' +
        'County Records\n' +
        'A register of deeds department, often located in a county courthouse, keeps residential records from the beginning of a property\'s history. While these records typically do not contain the names of the builders, they will list previous owners. From there, you can try to locate a former owner and ask who the builder was. Some communities are putting their records online, but the older your house is, the more likely it is that you\'ll have to go through the old record books in person or pay a fee for the clerk to conduct a search.\n' +
        '\n' +
        'Library Holdings\n' +
        'By knowing the year in which a historical house was built, which should appear on the title insurance documents or abstract of title, you can search the library\'s microfilmed copies of the local newspaper for that year to see if any residential developments, and builders, appear in news of local interest. Ask the librarian about any additional records, such as fire insurance maps or local architectural publications that might also list builders.\n' +
        '\n' +
        'House-Style Sleuthing\n' +
        'If other avenues fail to produce leads, contacting the owners of homes similar in style to yours might turn up clues. For instance, if your home has a barn-style roof with a large front porch and another home in the community is identical in style and age, the same builder might have constructed both homes.\n' +
        '\n' +
        'Memories and Memoirs\n' +
        'Elderly neighbors, a historical society and town historians are all sources of decades-old local information. Local libraries carry historic books by local authors that you can browse on the off chance your historical home, or similar homes, appear. Unless your home is a landmark, though, its builder\'s history could be lost to the annals of time. Builder records were sketchy before the turn of the 19th century.');
}

function story5() {
    document.write('\n' +
        'I blame the Channel 4 programme Grand Designs. Like tens of thousands of others, I have built my own home. It cost more than I planned, took longer than expected and ended with an acrimonious visit to a lawyer. However, like childbirth, the pain was worth it. We have the home of our dreams with space, light and two dishwashers.\n' +
        '\n' +
        'Yet I should have heeded the warning signals. In the 18 years that Grand Designs has been on the air, the nation has thrilled to the sight of scores of design-stricken couples impoverishing themselves and jeopardising their mental health in quests to build ideal homes on less than ideal sites.\n' +
        '\n' +
        'In the latest series, one couple was forced to borrow from family members when they found their new country home with a view could not be connected to the electricity or water supply without spending an extra £40,000 to dig a long trench in the road. In Hertfordshire, a developer (who should have known better) spent six years fighting the planners and another four years building his Roman villa-inspired home. Costs over the 10 years rose from £600,000 to an estimated £2m.\n' +
        '\n' +
        'These tales of woe appear not to be putting people off. Self-builders now account for almost 10 per cent of private new-build homes in the UK. But there are a number of rules of thumb that can help smooth a home’s construction — or prevent an oversight from turning into a catastrophe.\n' +
        '\n' +
        'Be prepared\n' +
        'Most self-builds, it must be said, have relatively modest designs. The reason, according to James Blair, of the Self Build Mortgage Shop in Hertfordshire, is that the ambitious telegenic schemes we know and love would never get finance from mortgage lenders. Self-builders need outline or full planning permission to get initial funding. They also need a professional project manager, detailed costings and a builder that has been credit-searched and recommended.\n' +
        '\n' +
        'It takes a lot of patience and a large deposit for self-builders to qualify for a mortgage. They also need to build a home that can easily be valued, not a one-off masterpiece made from shipping containers on a steep slope with restricted access. They must also have enough cash to cover delays in the “stage payments” that mortgage lenders release to fund construction and expensive rates of interest during the build.\n' +
        '\n' +
        '\n' +
        'UK property prices, self build and pension transfers\n' +
        '\n' +
        '\n' +
        'I knocked down an existing house and started from scratch. There are tax advantages to building a new home instead of extending an existing one. New self-builds qualify for rebates on VAT. Kevin McCloud, the Grand Designs presenter, estimates that the typical cost of building your own home is £1,500-£2,000 per sq m — if you stick to your original design and specification. Changing plans can cost you dear.\n' +
        '\n' +
        'In 2011, as part of its Housing Strategy for England, the government announced that it expected to double the number of self-built properties, with 100,000 to be completed by 2021. Legislation in 2016 included several measures to facilitate self and custom build, placing a duty on councils to allocate land.\n' +
        '\n' +
        'Under the Housing and Planning Act, local authorities should consider how they can best support self-build. They must give development permission to enough suitable serviced plots of land to meet the demand for self-build and custom housebuilding in their area within the next three years. The level of demand is established by how many people register for the right to build in a year. The first “base period” finished at the end of last month.\n' +
        '\n' +
        'Councils vary in their willingness to help self-builders: a report on the sector by Research found local authorities in the Northeast and West Midlands were most helpful to self-builders.\n' +
        '\n' +
        'According to AMA Research, the biggest problems for self-builders are the availability of land, getting planning permission and accessing funding. Most self-build properties are high-specification detached homes — and a key growth area is home automation, intelligent houses that made it easier to co-ordinate solar panels with the use of appliances such as dishwashers and washing machines, ensuring that they only came on when the sun shines.\n' +
        '\n' +
        'Borrowing to build\n' +
        'Self-builders need more money up front than conventional homebuyers. They usually have to buy their building plots and fund their planning applications before they can get loans.\n' +
        '\n' +
        'Mortgages for self-builders tend to be interest-only. They work like an overdraft: the borrower pays interest when money is drawn down at the completion of each stage of the build. Cheap fixed-rate loans tend not to be available during the building process and there can be hefty exit fees imposed on borrowers who change loans when the work is completed. Self-build interest rates are typically more than 5 per cent.\n' +
        '\n' +
        'Self-builders should expect to take as long as six months to get their finance in place and have 25 per cent of the cost of the land and building materials upfront. Before they can apply for a mortgage, they also need to have full or outline planning permission. When the building is completed some self-build lenders will automatically offer a lower-rate conventional mortgage.\n' +
        '\n' +
        'Recommended\n' +
        'How to build your perfect country pile\n' +
        'UK property law: how to build your dream home\n' +
        'Living the dream in your self-build home\n' +
        'Lenders are guided by valuers because they do not want to lend on a property that is worth less than the loan. They need market evidence of the resale values of the properties they are about to build. This is especially true of prefabricated buildings and “kit” houses; they will ask for documented evidence of their long-term structural integrity and longevity.\n' +
        '\n' +
        'Most self-builders use brokers to find their loans. Mr Blair of the Self Build Mortgage Shop said: “Getting a loan is like pulling teeth now. It takes three to four months to get a decision in principle on the mortgage. It takes a minimum of £5,000 just to get the mortgage with lender’s fees, arrangement fees, valuation fees and planning permission.”\n' +
        '\n' +
        'At the outset, he sends potential clients more than 100 questions, including some on their spending and income. He receives new inquiries every day, but often he does not hear from the clients for another 18 months as they search for plots, builders and architects.\n' +
        '\n');
}