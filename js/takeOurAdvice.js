function createCityList() {
    var countrySelector = document.getElementById("country");
    var selectedCountry = countrySelector.options[countrySelector.selectedIndex].text;

    var citySelector = document.getElementById("city");
    removeOptions(citySelector);

    if (selectedCountry === "Romania") {
        var opt = document.createElement('option');
        opt.innerHTML = "Cluj-Napoca";
        citySelector.appendChild(opt);

        opt = document.createElement('option');
        opt.innerHTML = "Baia Mare";
        citySelector.appendChild(opt);

        opt = document.createElement('option');
        opt.innerHTML = "Bucuresti";
        citySelector.appendChild(opt);
    }
    else if (selectedCountry === "Deutschland") {
        opt = document.createElement('option');
        opt.innerHTML = "Berlin";
        citySelector.appendChild(opt);

        opt = document.createElement('option');
        opt.innerHTML = "Hamburg";
        citySelector.appendChild(opt);

        opt = document.createElement('option');
        opt.innerHTML = "Munich";
        citySelector.appendChild(opt);
    } else if (selectedCountry === "France") {
        opt = document.createElement('option');
        opt.innerHTML = "Paris";
        citySelector.appendChild(opt);

        opt = document.createElement('option');
        opt.innerHTML = "Lyon";
        citySelector.appendChild(opt);

        opt = document.createElement('option');
        opt.innerHTML = "Marseille";
        citySelector.appendChild(opt);
    } else if (selectedCountry === "Italy") {
        opt = document.createElement('option');
        opt.innerHTML = "Rome";
        citySelector.appendChild(opt);

        opt = document.createElement('option');
        opt.innerHTML = "Florence";
        citySelector.appendChild(opt);

        opt = document.createElement('option');
        opt.innerHTML = "Milan";
        citySelector.appendChild(opt);
    }
}

function createAdvice() {
    var citySelector = document.getElementById("city");
    var selectedCity = citySelector.options[citySelector.selectedIndex].text;
    var adviceText = document.getElementById("adviceText");
    var adviceZone = document.getElementById("advice");
    var adviceString;

    if (selectedCity === "Cluj-Napoca" ||
            selectedCity === "Berlin" ||
            selectedCity === "Paris" ||
            selectedCity === "Rome") adviceString = "Your obligation to family " +
        "and household is where your heaviest financial expense is now occurring. Some may have bought land, " +
        "or a new property and have additional monthly payments.\n" +
        "Fortunately if you work hard and plan your investment schedule in February " +
        "into early March you can come out on top. From September partnerships or unions can bring financial benefit.";

    if (selectedCity === "Baia Mare" ||
        selectedCity === "Hamburg" ||
        selectedCity === "Lyon" ||
        selectedCity === "Florence") adviceString = "Career takes a higher prominence now and you are inclined to want to put effort in " +
        "from very early in the year, and some could begin a new job. If change does come in this area it is likely late " +
        "April to May 2019 as someone may go back on a contract, or you choose to end it anyway.";

    if (selectedCity === "Bucuresti" ||
        selectedCity === "Munich" ||
        selectedCity === "Marseille" ||
        selectedCity === "Milan") adviceString = "This is an ideal year to let go of the past and modernize " +
        "many of your ways of doing things. The earlier in the year you realize this the easier " +
        "your life will be. You will only cause yourself unnecessary conflict around you by arguing " +
        "about old matters you can no longer change. March is the month to let go.";

    adviceText.innerText = adviceString;
    adviceZone.style.visibility = "visible";
}

function removeOptions(selectbox)
{
    var i;
    for(i = selectbox.options.length - 1 ; i >= 0 ; i--)
    {
        selectbox.remove(i);
    }
}