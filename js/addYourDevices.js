function findDevices() {
    var request = new XMLHttpRequest();
    request.open('GET', 'http://localhost:3000/' + "devices", true);
    request.onload = function () {
        var data = JSON.parse(this.response);
        if (request.status >= 200 && request.status < 400) {
            var devicesTable = document.getElementById("devicesTable");
            for (i in data){
                var device = data[i];
                console.log(device);
                devicesTable.innerHTML +=
                "<tr>" 
                    +"<td class=\"image-td\">" 
                    +    "<img src=" + device.icon + ">" 
                    +"</td>" 
                    +"<td class=\"name-td\">"  
                    +    "<p>" + device.name + "</p>" 
                    +"</td>" 
                    +"<td id="+ device.consumption + " class=\"description-td\">"
                    +    "<p>" + device.description + "</p>"
                    +"</td>" 
                    +"<td class=\"button-td\">" 
                    +    "<button class=\"btn btn-success add-button\" onclick=\"addDevice(this, " + device.consumption + ")\">ADD</button>"
                    +"</td>"
                +"</tr>";
            }
        } else {
            console.log('error');
        }
    }
    request.send();
}

function addDevice(element, deviceId){
    element.closest("td").innerHTML="<button class=\"btn remove-button\" onclick=\"removeDevice(this," + deviceId+ ")\">REMOVE</button>\n" +
                                    "<button class=\"btn btn-success\" onclick=\"onOff(this)\">TURN OFF</button>\n";
    document.getElementById(deviceId).innerHTML = "<p>CONSUMPTION = " + deviceId + " kW/h</p>";
}

function removeDevice(element, deviceId){
    var request = new XMLHttpRequest();
    request.open('GET', 'http://localhost:3000/' + "devices", true);
    request.onload = function () {
        var data = JSON.parse(this.response);
        if (request.status >= 200 && request.status < 400) {
            for (i in data){
                var device = data[i];
                if (device.consumption === deviceId){
                    document.getElementById(deviceId).innerHTML = "<p>" + device.description + "</p>"
                }
                console.log(device);
            }
        } else {
            console.log('error');
        }
    }
    request.send();
    element.closest("td").innerHTML = "<button class=\"btn btn-success add-button\" onclick=\"addDevice(this)\">ADD</button>";
}

function onOff(element){
    if (element.classList.contains("btn-success")){
        element.classList.remove("btn-success");
        element.classList.add("off-button");
        element.innerHTML = "TURN ON";
    } else if (element.classList.contains("off-button")){
        element.classList.remove("off-button");
        element.classList.add("btn-success");
        element.innerHTML = "TURN OFF";    
    }
}