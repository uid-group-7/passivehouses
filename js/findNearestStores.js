function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -33.8688, lng: 151.2195},
          zoom: 13,
          mapTypeId: 'roadmap'
        });

        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);

        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();
          console.log(places);

          if (places.length == 0) {
            return;
          }

          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            var marker = new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            });

            google.maps.event.addListener(marker, 'click', function() { 
               var modalTitle = document.getElementById('modal-title');
               var address = document.getElementById('address');
               var hours = document.getElementById('opening-hours');
               var rating = document.getElementById('rating');
               address.innerHTML = "<b>Location: </b>" + place.formatted_address;
               if(place.opening_hours.open_now == true)
               		hours.innerHTML = "<b>Open now</b>";
               else
              		hours.innerHTML = "<b>Closed now</b>";
               rating.innerHTML = "<b>Rating: </b>" + place.rating + " based on " + place.user_ratings_total + " reviews";
               modalTitle.innerHTML = "<img src='" + place.photos[0].getUrl({'maxWidth': 470, 'maxHeight': 470}) +"'>" + place.name;
		       $('#myModal').modal('show'); 
		    }); 
            markers.push(marker);

            if (place.geometry.viewport) {
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }