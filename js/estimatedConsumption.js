
window.onload = init;
var rIndex;
var monthlyConsumption = 0;


function init(){
    rIndex = 0;
    var removebutton = document.getElementById("removebtn");
	removebutton.onclick = removeSelectedRow;
}

function removeSelectedRow() {
    if (rIndex == 0){
        return null;
    }
    var table = document.getElementById("apptable");
    table.deleteRow(rIndex);
    rIndex--;
}

function createAppliance(){
    var table = document.getElementById("tableBody");
    var selectionInput = document.getElementById("appliance");
    var selectionValue = selectionInput.options[selectionInput.selectedIndex].value;
    table.innerHTML+="<tr>"+
                        "<td class=\"leftCol\">" + selectionValue + "</td>"+
                        "<td class=\"rightCol\"><input class=\"textInput numeric\" pattern=\"[0-9]\" type=\"number\" onblur=calculateConsumption(this)></td>"+
                    "</tr>";
    rIndex++;
} 

function calculateConsumption(field){
    var consumptionValue = field.value;
    monthlyConsumption += consumptionValue * 24 * 30;

    document.getElementById("currentMonth").innerHTML = "<b>ESTIMATED ENERGY CONSUMPTION: " + monthlyConsumption + "</b>";
}