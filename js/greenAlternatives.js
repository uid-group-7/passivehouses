function showAccordion() {
    var applianceSelector = document.getElementById("applianceType");
    var selectedAppliance = applianceSelector.options[applianceSelector.selectedIndex].text;

    var firstApplianceName = document.getElementById("firstApplianceName");
    var secondApplianceName = document.getElementById("secondApplianceName");
    var thirdApplianceName = document.getElementById("thirdApplianceName");

    var firstApplianceImage = document.getElementById("firstAppliancePhoto");
    var secondApplianceImage = document.getElementById("secondAppliancePhoto");
    var thirdApplianceImage = document.getElementById("thirdAppliancePhoto");

    var firstApplianceDescription = document.getElementById("firstApplianceDescription");
    var secondApplianceDescription = document.getElementById("secondApplianceDescription");
    var thirdApplianceDescription = document.getElementById("thirdApplianceDescription");

    if (selectedAppliance === "Fridge") {
        document.getElementById("errorMessage").style.visibility = "hidden";
        document.getElementById("myAccordion").style.visibility = "visible";

        firstApplianceName.innerHTML = "Whirlpool WRB322DMBM";
        secondApplianceName.innerHTML = "Whirlpool Double Drawer French Door Refrigerator";
        thirdApplianceName.innerHTML = "GE Side-by-Side Refrigerator";

        firstApplianceImage.setAttribute("src", "../resources/fridge1.jpg");
        firstApplianceImage.setAttribute("class", "applianceImg");

        secondApplianceImage.setAttribute("src", "../resources/fridge2.jpg");
        secondApplianceImage.setAttribute("class", "applianceImg");

        thirdApplianceImage.setAttribute("src", "../resources/fridge3.jpg");
        thirdApplianceImage.setAttribute("class", "applianceImg");

        firstApplianceDescription.innerHTML = "33 Inch Bottom-Freezer Refrigerator with 21.9 cu. ft. Capacity\n" +
            "                        Keep your food fresh and organized with Whirlpool's bottom-freezer refrigerator. Within the\n" +
            "                        refrigerator are four spill safe cantilever shelves, two crisper bins, four adjustable door\n" +
            "                        bins, and two fixed door bins. The freezer contains two full width sliding baskets and an ice\n" +
            "                        maker. LED lighting makes it easy to see fresh and frozen food. It puts a spotlight on food and\n" +
            "                        casts a\n" +
            "                        more natural light on the contents of your refrigerator so food looks as it should.\n";
        secondApplianceDescription.innerHTML = "Dual cooling\n" +
            "                        A standout feature of the Whirlpool Double Drawer French Door Refrigerator is its dual cooling\n" +
            "                        system, which detects the internal temperature of the fridge and adjusts humidity levels\n" +
            "                        automatically. The end result is fresher food and a lower chance of freezer burn.\n";
        thirdApplianceDescription.innerHTML = "Lots of freezer space\n" +
            "                        If you tend to keep your freezer well-stocked, a side-by-side refrigerator is likely the best\n" +
            "                        style for you — they offer the most freezer space of any style.\n" +
            "\n" +
            "                        The GE Side-by-Side Refrigerator has more than 30% more freezer space than our picks for the\n" +
            "                        best top or bottom freezer, and it still outcompetes the French door refrigerator for freezer\n" +
            "                        space. Additionally, these refrigerators provide shelving units for your freezer, helping it\n" +
            "                        stay more organized than any other style.\n";
    }

    if (selectedAppliance === "TV") {
        document.getElementById("errorMessage").style.visibility = "hidden";
        document.getElementById("myAccordion").style.visibility = "visible";

        firstApplianceName.innerHTML = "Samsung Q9FN QLED (2018)";
        secondApplianceName.innerHTML = "LG C8 OLED Series (2018)";
        thirdApplianceName.innerHTML = "Samsung Q900R QLED TV (2018)";

        firstApplianceImage.setAttribute("src", "../resources/tv1.jpg");
        firstApplianceImage.setAttribute("class", "applianceImg");

        secondApplianceImage.setAttribute("src", "../resources/tv2.jpg");
        secondApplianceImage.setAttribute("class", "applianceImg");

        thirdApplianceImage.setAttribute("src", "../resources/tv3.jpg");
        thirdApplianceImage.setAttribute("class", "applianceImg");

        firstApplianceDescription.innerHTML = "As well as being even brighter and more colorful than last year’s equivalent model, Samsung's 2018 flagship screens use a completely different lighting system to combat its predecessor’s contrast problems: Full Array Local Dimming rather than edge-lit LED lighting. The FALD panel works in tandem with Samsung QLED Quantum Dots to produce a picture that's brighter and more colorful than near any we've seen come from the South Korean manufacturer.";

        secondApplianceDescription.innerHTML = "At the top of our list for 2018 is the LG C8 OLED – available in both 55 and 65-inch iterations. It's here because it combines an impressive picture, an extensive set of features, an attractive design and its unrivaled smart platform, to deliver one of the best TVs we have seen to date. It’s not as bright as an LCD TV but those deep blacks make a huge difference to the dynamic range of the image. It’s also capable of vibrant and gorgeous colors, not to mention an astounding level of detail with native 4K content.";
        thirdApplianceDescription.innerHTML = "Its native 8K pictures are incredible, looking just like the real world - only better. But even more crucially given the dearth of true 8K content for the foreseeable future, the 85Q900R makes all today’s lower resolution sources look better than they do anywhere else, too.";
    }

    if (selectedAppliance === "Washing machine") {
        document.getElementById("myAccordion").style.visibility = "hidden";
        document.getElementById("errorMessage").style.visibility = "visible";

    }
}
