window.onload = init;
var rIndex;

function init(){
  populateList();
	var topicSel = document.getElementById("topic-sel");
	var sortbySel = document.getElementById("sortby-sel");
}

function createItem(article, index) {
  var string = "<li class=\"list-group-item\" onclick=\"openArticle(this," + index + ")\">" +
                                            "<div class=\"d-flex w-100 justify-content-between\">" +
                                              "<h5 class=\"mb-1\" id=\"titlu" + index + "\">" + article.title + "</h5>" +
                                              "<small id=\"date" + index + "\">" + article.date + "</small>" + 
                                            "</div>" + 
                                            "<p class=\"mb-1\">" + article.preview + "</p>" + 
                                            "<small id=\"author" + index + "\">" + article.author + "</small>" + 
                                            "<small hidden id=\"content" + index + "\">" + article.content + "</small>" + 
                                            "<div id=\"stars\">";
  for(var j = 0; j < article.stars; j++)
    string += "<span class=\"fa fa-star checked\"></span>";
  for(var j = 0; j < 5 - article.stars; j++)
    string += "<span class=\"fa fa-star\"></span>";
  string += "</div></li>";
  return string;
}

function populateList(){
  var articlesList = document.getElementById("articleList");
  articlesList.innerHTML = "";
  var xobj = new XMLHttpRequest();
        xobj.overrideMimeType("application/json");
    xobj.open('GET', 'http://localhost:3000/articles', true);
    xobj.onreadystatechange = function () {
          if (xobj.readyState == 4 && xobj.status == "200") {
            var data = JSON.parse(xobj.response);
            for (var i=0; i < data.length; i++) {
              var article = data[i];
              var string = createItem(article, i);
              articlesList.innerHTML += string;
            }
          }
    };
    xobj.send();
}

function sortByTopic(){
  var articlesList = document.getElementById("articleList");
  articlesList.innerHTML = "";
	var topicSel = document.getElementById("topic-sel");
	var topicSelected = topicSel.options[topicSel.selectedIndex].text;
	var xobj = new XMLHttpRequest();
        xobj.overrideMimeType("application/json");
    xobj.open('GET', 'http://localhost:3000/articles', true);
    xobj.onreadystatechange = function () {
          if (xobj.readyState == 4 && xobj.status == "200") {
            var data = JSON.parse(xobj.response);
            for (var i=0; i < data.length; i++) {
              var article = data[i];
              if(article.topic == topicSelected){
                var string = createItem(article, i);
                articlesList.innerHTML += string;
              }
          }
    };
  }
    xobj.send();  
}

function sortListBy() {
  var lis = document.getElementById("articleList").getElementsByTagName("h5");
  var existingItems = [];
  for(var i = 0; i < lis.length; i++)
    existingItems.push(lis[i].innerText);
  console.log(existingItems);
  var articlesList = document.getElementById("articleList");
  articlesList.innerHTML = "";
  var criteriaSel = document.getElementById("sortby-sel");
  var criteriaSelected = criteriaSel.options[criteriaSel.selectedIndex].text;
  var xobj = new XMLHttpRequest();
        xobj.overrideMimeType("application/json");
    xobj.open('GET', 'http://localhost:3000/articles', true);
    xobj.onreadystatechange = function () {
          if (xobj.readyState == 4 && xobj.status == "200") {
            var data = JSON.parse(xobj.response);
            var sortedArticles;
            if(criteriaSelected == "Newest")
              sortedArticles = sortByDate(data);
            else if(criteriaSelected == "Rating")
              sortedArticles = sortByRating(data);
            for (var i=0; i < sortedArticles.length; i++) {
              var article = sortedArticles[i];
              if(existingItems.includes(article.title)){
                var string = createItem(article, i);
                articlesList.innerHTML += string;
            }
              
              
          }
    };
  }
    xobj.send();  
}

function sortByRating(articles){
  articles.sort(function(a, b){return b.stars - a.stars});
  return articles;
}

function sortByDate(articles){
  articles.sort(function(a, b){return new Date(b.date) - new Date(a.date)});
  return articles;
}

function openArticle(e, index) {
    var titlu = document.getElementById("modal-title");
    titlu.innerText = document.getElementById("titlu" + index).innerText;

    var content = document.getElementById("modal-content");
    content.innerText = document.getElementById("content" + index).innerText;

    var author = document.getElementById("modal-author");
    author.innerText = document.getElementById("author" + index).innerText;
     
    $('#articleModal').modal('show');       
  }

  function starchange(item)
{
count=item.id[0];
sessionStorage.starRating = count;
var subid= item.id.substring(1);

for(var i=0;i<5;i++)
{
if(i<count)
{
document.getElementById(i+1).style.color="orange";
}
else
{
document.getElementById(i+1).style.color="black";
}
}

}

function starmark(item)
{
  voted++;
count=item.id[0];
sessionStorage.starRating = count;
var subid= item.id.substring(1);
generalRating=(generalRating+count)/voted;
for(var i=0;i<5;i++)
{
if(i<count)
{
document.getElementById(i+1).style.color="orange";
}
else
{
document.getElementById(i+1).style.color="black";
}
}
}